//-----------------------------------------------------------------------

// <copyright file="test_air.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include <cmath>
#include "catch.hpp"

#include "air.h"

using namespace mw;

TEST_CASE("Air", "[air]") {
  Air air;
  REQUIRE(fabs(air.X("N2") - 0.7812) < 1.E-6);
  REQUIRE(fabs(air.X("O2") - 0.2095) < 1.E-6);

  air.set_H2Od(0.010);
  REQUIRE(fabs(air.X("N2") - 0.7688) < 1.0E-4);
  REQUIRE(fabs(air.X("O2") - 0.2062) < 1.0E-4);
  REQUIRE(fabs(air.X("Ar") - 0.0092) < 1.0E-4);

  air.set_H2Od(0.);
  REQUIRE(fabs(air.X("N2") - 0.7812) < 1.E-6);
  REQUIRE(fabs(air.X("O2") - 0.2095) < 1.E-6);
}
