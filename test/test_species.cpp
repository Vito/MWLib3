//-----------------------------------------------------------------------

// <copyright file="test_species.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#define CATCH_CONFIG_MAIN

#include <cmath>

#include "catch.hpp"

#include "constants.h"
#include "elements.h"
#include "species.h"

TEST_CASE("Species - molmasses", "[species]")
{
	using sp = mw::Species;
	using el = mw::Elements;

	double M_O2 = sp::mwt("O2");
	double M_O = el::mwt("O");
	REQUIRE(M_O2 == 2 * M_O);

	REQUIRE( sp::mwt("O2") == 2*el::mwt("O") );
	REQUIRE( sp::mwt("N2") == 2*el::mwt("N") );
	REQUIRE( sp::mwt("CO2") == el::mwt("C")+2*el::mwt("O") );
	REQUIRE( sp::mwt("H2O") == 2*el::mwt("H")+el::mwt("O") );
	//REQUIRE( sp::mwt("SO2") == el::mwt("S")+2*el::mwt("O") );


    double R_CO2 = sp::R("CO2");
    REQUIRE(fabs(R_CO2 - mw::Rm/(44.01*1.0E-3)) <= 1.0E-2 );
}

TEST_CASE("Species - viscosities", "[species]")
{
    using sp = mw::Species;

    REQUIRE( fabs(sp::dynamicGasViscosity("CO2", 273.15)*1E6 - 13.71) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("CO2", 1173.15)*1E6 - 46.06) < 0.5 );


    REQUIRE( fabs(sp::dynamicGasViscosity("N2", 273.15)*1E6 - 16.63) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("N2", 1273.15)*1E6 - 48.6) < 0.5 );

    REQUIRE( fabs(sp::dynamicGasViscosity("H2O", 273.15)*1E6 - 8.945) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("H2O", 1073.15)*1E6 - 40.43) < 0.5 );

    REQUIRE( fabs(sp::dynamicGasViscosity("O2", 273.15)*1E6 - 19.14) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("O2", 973.15)*1E6 - 48.24) < 0.5 );

    REQUIRE( fabs(sp::dynamicGasViscosity("Ar", 273.15)*1E6 - 21.1) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("Ar", 773.15)*1E6 - 46.5) < 0.5 );

    REQUIRE( fabs(sp::dynamicGasViscosity("SO2", 273.15)*1E6 - 11.8) < 0.5 );
    REQUIRE( fabs(sp::dynamicGasViscosity("SO2", 773.15)*1E6 - 31.3) < 0.5 );
}

TEST_CASE("Species - thermal conductivities", "[species]")
{
    using sp = mw::Species;

    REQUIRE( fabs(sp::thermalGasConductivity("CO2", 273.15)*1E3 - 14.67) < 0.5 );
    REQUIRE( fabs(sp::thermalGasConductivity("CO2", 1173.15)*1E3 - 81.59) < 0.5 );


    REQUIRE( fabs(sp::thermalGasConductivity("N2", 273.15)*1E3 - 24) < 0.5 );
    REQUIRE( fabs(sp::thermalGasConductivity("N2", 1273.15)*1E3 - 77.99) < 0.5 );

    REQUIRE( fabs(sp::thermalGasConductivity("H2O", 273.15)*1E3 - 16.76) < 0.55 );
    REQUIRE( fabs(sp::thermalGasConductivity("H2O", 1073.15)*1E3 - 105.8) < 0.5 );

    REQUIRE( fabs(sp::thermalGasConductivity("O2", 273.15)*1E3 - 24.35) < 0.5 );
    REQUIRE( fabs(sp::thermalGasConductivity("O2", 973.15)*1E3 - 70.04) < 0.5 );

    REQUIRE( fabs(sp::thermalGasConductivity("Ar", 273.15)*1E3 - 16.6) < 0.5 );
    REQUIRE( fabs(sp::thermalGasConductivity("Ar", 773.15)*1E3 - 36.2) < 0.5 );

    REQUIRE( fabs(sp::thermalGasConductivity("SO2", 273.15)*1E3 - 8.4) < 0.5 );
    REQUIRE( fabs(sp::thermalGasConductivity("SO2", 773.15)*1E3 - 34.3) < 0.5 );
}
