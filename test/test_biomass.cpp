//-----------------------------------------------------------------------

// <copyright file="test_biomass.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include <cmath>
#include <iostream>

#include "catch.hpp"
#include "biomass.h"
#include "fluegas.h"

TEST_CASE("Testing Biomass", "[biomass]")
{
    using namespace mw;

    Biomass bio;
    bio.set_W(0.15);
    bio.set_A_wf(0.04);
    bio.set_Y_waf({{"C",0.51},{"H",0.06},{"S",0.0003},{"O",0.4247},{"N",0.005}});

    SECTION("Biomass composition") {
        REQUIRE(fabs(bio.Y("C")-0.4162) <= 1.0E-4);
        REQUIRE(fabs(bio.Y("O")-0.3466) <= 1.0E-4);
        REQUIRE(fabs(bio.Y("H")-0.0490) <= 1.0E-4);
        REQUIRE(fabs(bio.Y("S")-0.0002) <= 1.0E-4);
        REQUIRE(fabs(bio.Y("N")-0.0041) <= 1.0E-4);
        REQUIRE(fabs(bio.Y("W")-0.15) <= 1.0E-4);

        REQUIRE(fabs(bio.Y_wf("C")-0.4896) <= 1.0E-4);
        REQUIRE(fabs(bio.Y_wf("O")-0.4077) <= 1.0E-4);
        REQUIRE(fabs(bio.Y_wf("H")-0.0576) <= 1.0E-4);
        REQUIRE(fabs(bio.Y_wf("S")-0.0003) <= 1.0E-4);
        REQUIRE(fabs(bio.Y_wf("N")-0.0048) <= 1.0E-4);
    }

    SECTION("GCV dry") {
        double gcv = bio.GCV_dry();
        double ncv = bio.NCV_wet();
        REQUIRE(fabs(gcv-19574448.32) <= 1.0E-2);
        REQUIRE(fabs(ncv-15202415.04) <= 1.0E-2);
    }

    Air air;

    SECTION("Lambda from O2 dry") {
        double x_O2dry = 6.0/100.;
        double lambda = bio.lambda_from_O2dry(x_O2dry, air);
        REQUIRE(fabs(lambda-1.3983) <= 1.0E-4);
    }

    SECTION("Lambda from O2 wet") {
        double x_O2wet = 6.0/100.;
        double lambda = bio.lambda_from_O2wet(x_O2wet, air);
        REQUIRE(fabs(lambda-1.4745) <= 1.0E-4);
    }

    SECTION("Fluegas composition and amount from lambda with dry air") {
        double lambda = 1.4;
        str_to_dbl nu;
        double moles_fg = bio.fluegas_from_lambda(lambda,air,nu);
        Fluegas fg;
        fg.set_X(nu);

        REQUIRE(fabs(fg.X("CO2")-0.1275) <= 1.0E-4);
        REQUIRE(fabs(fg.X("H2O")-0.1201) <= 1.0E-4);
        REQUIRE(fabs(fg.X("O2")-0.052961) <= 1.0E-4);
        REQUIRE(fabs(fg.X("N2")-0.6912) <= 1.0E-4);


        REQUIRE(fabs(fg.Y("CO2")-0.1925) <= 1.0E-4);
        REQUIRE(fabs(fg.Y("H2O")-0.0742) <= 1.0E-4);
        REQUIRE(fabs(fg.Y("O2")-0.0581) <= 1.0E-4);
        REQUIRE(fabs(fg.Y("N2")-0.6639) <= 1.0E-4);
        REQUIRE(fabs(fg.Y("SO2")-0.0001) <= 1.0E-4);

        REQUIRE(fabs(moles_fg-7.841) <= 1.0E-3);
    }

    SECTION("Fluegas composition and amount from lambda with wet air") {
        air.set_H2Od(0.010);
        double lambda = 1.4;
        str_to_dbl nu;
        double moles_fg = bio.fluegas_from_lambda(lambda,air,nu);
        Fluegas fg;
        fg.set_X(nu);

        REQUIRE(fabs(fg.X("CO2")-0.1257) <= 1.0E-4);
        REQUIRE(fabs(fg.X("H2O")-0.1324) <= 1.0E-4);
        REQUIRE(fabs(fg.X("O2")-0.052218) <= 1.0E-4);
        REQUIRE(fabs(fg.X("N2")-0.6815) <= 1.0E-4);

        REQUIRE(fabs(moles_fg-7.952) <= 1.0E-3);
    }


    SECTION("Oxygen stoichometric") {
        double moleO2_stoich  = bio.moleO2stoich_per_moleC();
        REQUIRE(fabs(moleO2_stoich - 1.038) <= 1.0e-3);
    }

}
