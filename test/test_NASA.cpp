//-----------------------------------------------------------------------

// <copyright file="test_NASA.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include <cmath>

#include "catch.hpp"

#include "NASA.h"

using namespace mw;

TEST_CASE("NASA", "[NASA]") {
	NASA g;

	std::map<std::string,double> x;
	x["N2"] = 1.0;

	g.set_TP(25+273.15,1.0E5);
	g.set_X(x);
	double h1 = g.H_mass();
    REQUIRE( fabs(h1 - 0.0) < 1.0);

	g.set_T(100.+273.15);
	double h2 = g.H_mass();

    auto mmw = g.mmw();   // g/mol or kg/kmol

    REQUIRE( fabs(mmw - 28.0134) < 1E-3);

    auto h2_J_molK = h2 * mmw/1000;
    REQUIRE( fabs(h2_J_molK - 2187.227) < 0.1);


    REQUIRE( fabs(h2 - 78078) < 2 );

}
