//-----------------------------------------------------------------------

// <copyright file="test_fluegas.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "catch.hpp"

#include <iostream>

#include "biomass.h"
#include "constants.h"
#include "fluegas.h"

TEST_CASE("Testing fluegas", "[fluegas]")
{
    using namespace mw;

    Biomass bio;
    bio.set_W(0.15);
    bio.set_A_wf(0.04);
    bio.set_Y_waf({{"C",0.51},{"H",0.06},{"S",0.0003},{"O",0.4247},{"N",0.005}});

    Air air;
    double lambda = 1.4;
    str_to_dbl nu;
    double moles_fg = bio.fluegas_from_lambda(lambda,air,nu);
    Fluegas fg;
    fg.set_X(nu);

    double p = one_atm;

    double T1 = 25 + 273.15;
    fg.set_TP(T1,p);
    double h1 = fg.H_mass();

    double T2 =  T1 + 100;
    fg.set_T(T2);
    double h2 = fg.H_mass();

    REQUIRE( h2-h1 > 0 );

    std::cout << h2-h1 <<std::endl;
}
