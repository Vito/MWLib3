//-----------------------------------------------------------------------

// <copyright file="air.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "air.h"

namespace mw {

Air::Air()
{
  str_to_dbl x;
  x["N2"] = 0.7812;
  x["O2"] = 0.2095;
  x["Ar"] = 0.0093;
  set_X(x);
}

std::unique_ptr<Gas> Air::clone() const {
    auto p = std::unique_ptr<Air>(new Air);
    if (p) {
        *p = *this;
        return std::move(p);
    } else {
        return nullptr;
    }
}


void Air::set_H2Od(double ud)
{
  // ud: kg H2O / kg dry air
  double yH2O_old = Y("H2O");
  double yH2O_new = ud / (1.0 + ud);
  double f = (1. - yH2O_new) / (1. - yH2O_old);
  str_to_dbl ynew;
  ynew["H2O"] = yH2O_new;
  for (auto& s : m_X)
    if (s.first != "H2O") ynew[s.first] = f * Y(s.first);

  set_Y(ynew);
}

}  // namespace mw
