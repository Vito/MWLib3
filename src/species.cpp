//-----------------------------------------------------------------------

// <copyright file="species.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "species.h"
#include "constants.h"
#include "elements.h"

namespace mw {

double Species::mwt(const std::string& k)
{
	static const auto mwts = create_mwts();
	return mwts.at(k);
}

double Species::R(const std::string& k)
{
    double mw = mwt(k)*1.0E-3;  // kg/mole
    return Rm/mw;
}

str_to_dbl Species::create_mwts()
{
	using el = Elements;
	str_to_dbl mwts;
	mwts["CO2"] = el::mwt("C")+2*el::mwt("O");
	mwts["O2"] = 2*el::mwt("O");
	mwts["SO2"] = el::mwt("S")+2*el::mwt("O");
	mwts["N2"] = 2*el::mwt("N");
	mwts["H2O"] = 2*el::mwt("H")+el::mwt("O");
	mwts["Ar"] = el::mwt("Ar");
	return mwts;
}


std::map<std::string, Species::QuadraticCorrelation> Species::createGasDynamicViskosityMap()
{
    std::map<std::string, QuadraticCorrelation> m;

    m["CO2"] = QuadraticCorrelation(-0.00001311, 0.05456866, 0.07716111);
    m["O2"] = QuadraticCorrelation(-0.00001614, 0.0611774, 3.83707206 );
    m["SO2"] = QuadraticCorrelation(-0.00001157, 0.05118394, -1.33828278);
    m["N2"] = QuadraticCorrelation(-0.00001012, 0.04697978, 4.90789137);
    m["H2O"] = QuadraticCorrelation(0.00000141, 0.03824936, -2.02527752);
    m["Ar"] = QuadraticCorrelation(-0.00002007, 0.07176295, 3.00122116);

    return m;
}

std::map<std::string, Species::QuadraticCorrelation> Species::createGasThermalConductivityMap()
{
    std::map<std::string, QuadraticCorrelation> m;

    m["CO2"] = QuadraticCorrelation(-0.00001464, 0.09594208, -10.75371285);
    m["O2"] = QuadraticCorrelation(-0.00001604, 0.084772, 2.57995754);
    m["SO2"] = QuadraticCorrelation(0.00000497, 0.04717253, -5.00220886);
    m["N2"] = QuadraticCorrelation(-0.00001289, 0.07308712, 5.4676709);
    m["H2O"] = QuadraticCorrelation(0.00004166, 0.05645226, -2.29011981);
    m["Ar"] = QuadraticCorrelation(-0.00000766, 0.0471527, 4.31897814);

    return m;
}


double Species::dynamicGasViscosity(const std::string& k, double T_Kelvin)
{
    static auto etaMap = createGasDynamicViskosityMap();
    return etaMap.at(k)(T_Kelvin)*1E-6;
}

double Species::thermalGasConductivity(const std::string& k, double T_Kelvin)
{
    static auto lambdaMap = createGasThermalConductivityMap();
    return lambdaMap.at(k)(T_Kelvin)*1E-3;
}

}  // namespace mw

