//-----------------------------------------------------------------------

// <copyright file="NASA_poly1.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  NASA_poly1_INC
#define  NASA_poly1_INC


#include <array>
#include <utility>

namespace mw {


class NASA_poly1
{
public:
	NASA_poly1()
		: m_Tmin(0.),
		  m_Tmax(0.)
	{
		for (int i=0; i<7; i++) m_a[i]=0;
	}

  NASA_poly1(double Tmin,
             double Tmax,
             double a[7])
  : m_Tmin(Tmin),
    m_Tmax(Tmax)
  {
    for (int i=0; i<7; i++)
      m_a[i] = a[i];
  }

	void set_a(double a[7])
	{
		for (int i=0; i<7; i++) m_a[i] = a[i];
	}

    double coeff(unsigned int i) const
    {
        return m_a[i];
    }

	void set_T_minmax(double Tmin, double Tmax)
	{
		m_Tmin = Tmin;
		m_Tmax = Tmax;
	}

  double Tmin() const { return m_Tmin; }
  double Tmax() const { return m_Tmax; }



  double Cp_R(double T) const;
  double S_R(double T) const;

  double H_RT(double T) const;
  double calc_T_from_H_R(double H_R_set) const;

  double H_R_min() const;  // get H/R at Tmin
  double H_R_max() const;  // Get H/R at Tmax

  double Hf298() const;

private:
  struct Ftor_T_from_H_R
  {
      Ftor_T_from_H_R(const std::array<double,7>& a, double H_R_set)
          : m_a(a),
            m_H_R_set(H_R_set) {}

      std::pair<double,double> operator()(const double& T) const
      {
        double T2 = T*T;
        double T3 = T*T2;
        double T4 = T*T3;
        auto& a = m_a;
        double H_RT = a[0]+a[1]*T/2 + a[2]*T2/3
                    + a[3]*T3/4 + a[4]*T4/5 + a[5]/T;

        double H_R = T*H_RT;

        double f = H_R - m_H_R_set;
        double df = a[0] + a[1]*T + a[2]*T2 + a[3]*T3 + a[4]*T4;

        return std::make_pair(f,df);
      }

      private:
      std::array<double,7> m_a;
      double m_H_R_set;
  };

private:
  double m_Tmin;
  double m_Tmax;
  double m_a[7];
};

}  // namespace mw

#endif   /* ----- #ifndef NASA_poly1_INC  ----- */
