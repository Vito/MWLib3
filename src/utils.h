//-----------------------------------------------------------------------

// <copyright file="utils.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  UTILS_INC
#define  UTILS_INC

#include <cctype>

#include <vector>
#include "common_types.h"

namespace mw {

void normalize(str_to_dbl& x);

void copy_value_or_zero(const str_to_dbl& x_in,
											  const std::vector<std::string>& species,
											  str_to_dbl& x_out);

std::string uppercase(const std::string& s); 

}  // namespace mw

#endif   /* ----- #ifndef UTILS_INC  ----- */
