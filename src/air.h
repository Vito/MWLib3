//-----------------------------------------------------------------------

// <copyright file="air.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  AIR_INC
#define  AIR_INC

#include "NASA.h"

namespace mw {

class Air : public NASA
{
public:
  Air();
  std::unique_ptr<Gas> clone() const override;

  void set_H2Od(double ud);

private:
};

}  // namespace mw

#endif   /* ----- #ifndef AIR_INC  ----- */
