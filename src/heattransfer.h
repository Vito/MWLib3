//-----------------------------------------------------------------------

// <copyright file="heattransfer.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:17:49 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef HEATTRANSFER_H
#define HEATTRANSFER_H

#include <vector>

#include <boost/math/tools/roots.hpp>
using boost::math::policies::policy;
using boost::math::tools::newton_raphson_iterate;
using boost::math::tools::halley_iterate; //
using boost::math::tools::eps_tolerance; // Binary functor for specified number of bits.
using boost::math::tools::bracket_and_solve_root;
using boost::math::tools::toms748_solve;

#include "fluegas.h"

namespace heattransfer {

namespace fluegaspipes {

constexpr auto PI = 3.14159265358979323846;
constexpr auto EMISSIVITY_WALL = 0.3;

struct SinglePass {
public:
    double da_m() const { return m_da_m; }
    double L_m() const { return m_L_m; }
    double di_m() const { return m_di_m; }
    double s_m() const { return m_s_m; }

    double Ai_m2()const { return m_numPipes * PI * m_di_m * m_L_m; }

    double Aa_m2() const { return m_numPipes * PI * m_da_m * m_L_m; }

    double Aq_m2() const { return m_numPipes * PI * m_di_m * m_di_m / 4; }

    void setNumPipes(double n) { m_numPipes = n; }
    double numPipes() const { return m_numPipes; }

    double sgl_m() const { return m_sgl_m; }

    double zetaFactorIn() const { return m_zetaFactorIn; }
    void zetaFactorIn(double z) { m_zetaFactorIn = z; }

    double zetaFactorOut() const { return m_zetaFactorOut; }
    void zetaFactorOut(double z) { m_zetaFactorOut = z; }

    void set_da_s_L_mm(double da, double s, double L)
    {
        m_da_m = da/1000;
        m_s_m = s/1000;
        m_L_m = L/1000;

        m_di_m = (da-2*s)/1000;
        m_sgl_m = 0.9 * m_di_m;
    }

    void set_da_s_L_m(double da, double s, double L)
    {
        m_da_m = da;
        m_s_m = s;
        m_L_m = L;

        m_di_m = (da-2*s);
        m_sgl_m = 0.9*m_di_m;
    }

    void set_radiationLossesWatt(double rlosses)
    {
        m_radiationLossesWatt = rlosses;
    }

    double get_radiationLossesWatt() const
    {
        return m_radiationLossesWatt;
    }

    double absRoughness_m() const { return m_absRoughness_m; }
    void absRoughness_m(double ar_m) { m_absRoughness_m = ar_m; }
    double relRoughness() const { return absRoughness_m() / di_m(); }

    double clearnessFactor() const { return m_clearnessFactor; }
    void clearnessFactor(double cf) { m_clearnessFactor = cf; }

    bool equals(const SinglePass& rhs)
    {
        bool eq = m_da_m == rhs.m_da_m
                && m_s_m == rhs.m_s_m
                && m_L_m == rhs.m_L_m
                && m_numPipes == rhs.m_numPipes;
        return eq;
    }

private:
    double m_numPipes = 1;
    double m_da_m = 60.3;
    double m_s_m = 3.6;
    double m_L_m = 3500;
    double m_di_m = 52.9;
    double m_sgl_m = 52.9;        	     // gleichwertige Schichtdicke
    double m_absRoughness_m = 0.001; // absolute rougness in m
    double m_zetaFactorIn = 2;//1.245           // zeta factor on inlet for dP calculation
    double m_zetaFactorOut = 1.0; //1.245;          // zeta factor on outlet for dP calculation
    double m_clearnessFactor = 1.0;

    double m_radiationLossesWatt = 0;
};

struct SinglePassFctor {
public:
    SinglePassFctor(const SinglePass& pass,
                    double mdotFg,
                    const mw::Gas& fg,
                    double Tfg_in_K,
                    double Tmedium_in_K,
                    double Tmedium_out_K,
                    double alpha_rad,
                    double alpha_a,
                    double dP)
        : m_mdotGas(mdotFg),
          m_fg(fg.clone()),
          m_Tfg_in(Tfg_in_K),
          m_Tmedium_in(Tmedium_in_K),
          m_Tmedium_out(Tmedium_out_K),
          m_alpha_i_rad(alpha_rad),
          m_alpha_a(alpha_a),
          m_dP(dP)
    {
        nPipes = pass.numPipes();
        di = pass.di_m();
        da = pass.da_m();
        L = pass.L_m();
        Ai = pass.Ai_m2();
        Aa = pass.Aa_m2();

        clearnessFactor = pass.clearnessFactor();
        radiationLossesWatt = pass.get_radiationLossesWatt();

        phi = m_mdotGas / pass.Aq_m2();
        fg_in = fg.clone();
        fg_out = fg.clone();
        fg_m = fg.clone();

        m_Tmedium_mid = (m_Tmedium_in + m_Tmedium_out) / 2;
    }

    SinglePassFctor(const SinglePassFctor& spf)
        : m_mdotGas(spf.m_mdotGas),
          m_fg(spf.m_fg->clone()),
          m_Tfg_in(spf.m_Tfg_in),
          m_Tmedium_in(spf.m_Tmedium_in),
          m_Tmedium_out(spf.m_Tmedium_out),
          m_alpha_i_rad(spf.m_alpha_i_rad),
          m_alpha_a(spf.m_alpha_a),
          m_dP(spf.m_dP),
          nPipes(spf.nPipes),
          di(spf.di),
          da(spf.da),
          L(spf.L),
          Ai(spf.Ai),
          Aa(spf.Aa),
          clearnessFactor(spf.clearnessFactor),
          radiationLossesWatt(spf.radiationLossesWatt),
          phi(spf.phi),
          fg_in(spf.fg_in->clone()),
          fg_out(spf.fg_out->clone()),
          fg_m(spf.fg_m->clone()),
          m_Tmedium_mid(spf.m_Tmedium_mid)
    {}


    double operator()(double T_Kelvin);

    private:
        double m_mdotGas;
        std::unique_ptr<mw::Gas> m_fg;
        double m_Tfg_in;
        double m_Tmedium_in;
        double m_Tmedium_out;
        double m_alpha_i_rad;  // radiative heat transfer coefficient inside tubes
        double m_alpha_a;    // heat transfer coefficient outside tubes
        double m_dP;

    private:
        double nPipes;
        double di;
        double da;
        double L;
        double Ai;    // inner tube area m2  (all tubes)
        double Aa;   // outer tube area m2   (all tubes)
        double clearnessFactor;
        double radiationLossesWatt;
        double phi;   // mass flux

        std::unique_ptr<mw::Gas> fg_in;
        std::unique_ptr<mw::Gas> fg_out;
        std::unique_ptr<mw::Gas> fg_m;

        double m_Tmedium_mid;
};


struct InsideWallTempFctor {
    InsideWallTempFctor(const mw::Gas& fg,
                        double qRad,
                        double Tg,
                        double sgl)
        : m_gas{fg.clone()},
          m_qRad{qRad},
          m_Tg{Tg},
          m_sgl{sgl}
    {
        m_psglCO2 = m_gas->P()/1E5 * m_gas->X("CO2") * m_sgl;  // bar*m
        m_psglH2O = m_gas->P()/1E5 * m_gas->X("H2O") * m_sgl;
    }

    double operator()(double Twall);

private:
    std::unique_ptr<mw::Gas> m_gas;
    double m_qRad;
    double m_Tg;
    double m_sgl;          // gleichwertige Schichtdicke [m]

private:
    double m_epsilonWall = EMISSIVITY_WALL;
    double m_psglCO2;
    double m_psglH2O;
};

std::unique_ptr<mw::Gas> solveAndGetGasOutTemperature(const SinglePass& pass,
                                    double mdotFg,
                                    const mw::Gas& fg,
                                    double Tmedium_in_K,
                                    double Tmedium_out_K);

std::unique_ptr<mw::Gas> solveAndGetGasOutTemperature(const std::vector<SinglePass>& passes,
                                    double mdotFg,
                                    const mw::Gas& fg,
                                    double Tmedium_in_K,
                                    double Tmedium_out_K);

double Num_Laminar(double Re, double Pr, double di, double L);
double Num_Turbulent(double Re, double Pr, double di, double L);
double Num(double Re, double Pr, double di, double L);

double qRadiation(double psglCO2,    // bar*m
                  double psglH2O,    // bar*m
                  double Tg,         // middle gas temperature K sqrt(T1*T2)
                  double Twall,      // mittle wall temperature K sqrt(T1*T2)
                  double eWall);     // wall emissitivity

double logTempDifference(double Tg_1, double Tg_2,
            double Tmed_1, double Tmed_2);

double emissCO2(double psgl, double Tg);
double emissH2O(double psgl, double Tg);
void emiss_CO2_H2O(double psglCO2,
                   double psglH2O,
                   double Tg,
                   double Tw,
                   double& eg,
                   double& av);

}  // fluegaspipes

}  // namespace heattransferk

#endif // HEATTRANSFER_H

