//-----------------------------------------------------------------------

// <copyright file="NASA_poly1.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include <cmath>
#include <limits>

#include <boost/math/tools/roots.hpp>
using boost::math::policies::policy;
using boost::math::tools::newton_raphson_iterate;
using boost::math::tools::halley_iterate;
using boost::math::tools::eps_tolerance; //

#include "NASA_poly1.h"
#include "constants.h"

namespace mw {

double NASA_poly1::Cp_R(double T) const
{
  double T2 = T*T;
  double T3 = T*T2;
  double T4 = T*T3;
  return m_a[0]+m_a[1]*T+m_a[2]*T2+m_a[3]*T3+m_a[4]*T4;
}

double NASA_poly1::S_R(double T) const
{
  double T2 = T*T;
  double T3 = T*T2;
  double T4 = T*T3;
  return m_a[0]*log(T)+m_a[1]*T+m_a[2]*T2/2+m_a[3]*T3/3+m_a[4]*T4/4+m_a[6];
}

double NASA_poly1::H_RT(double T) const
{
  double T2 = T*T;
  double T3 = T*T2;
  double T4 = T*T3;
  return m_a[0]+m_a[1]*T/2+m_a[2]*T2/3+m_a[3]*T3/4+m_a[4]*T4/5+m_a[5]/T;
}

double NASA_poly1::H_R_min() const
{
    double H_RT_min = H_RT(m_Tmin);
    return  m_Tmin * H_RT_min;
}

double NASA_poly1::H_R_max() const
{
    double H_RT_max = H_RT(m_Tmax);
    return  m_Tmax * H_RT_max;
}

double NASA_poly1::calc_T_from_H_R(double H_R_set) const
{
    std::array<double, 7> coeffs;
    for (int i = 0; i < 7; i++) coeffs[i] = m_a[i];

    Ftor_T_from_H_R ftor(coeffs, H_R_set);

    double hR_min = H_R_min();
    double hR_max = H_R_max();

    double guess = m_Tmin + (H_R_set-hR_min)/(hR_max-hR_min)*(m_Tmax-m_Tmin);
    if (guess < m_Tmin) guess = m_Tmin;
    else if (m_Tmax < guess) guess = m_Tmax;

    int digits = std::numeric_limits<double>::digits; // Maximum possible binary digits accuracy for type T.
    // digits used to control how accurate to try to make the result.
    int get_digits = (digits * 3) /4; // Near maximum (3/4) possible accuracy.

    //boost::uintmax_t maxit = (std::numeric_limits<boost::uintmax_t>::max)();
    // the default (std::numeric_limits<boost::uintmax_t>::max)() = 18446744073709551615
    // which is more than we might wish to wait for!!!  so we can reduce it
    boost::uintmax_t maxit = 20;
    //cout << "Max Iterations " << maxit << endl; //
    double result = newton_raphson_iterate(ftor, guess, m_Tmin, m_Tmax, get_digits, maxit);
    // Can show how many iterations (updated by newton_raphson_iterate) but lost on exit.
    // cout << "Iterations " << maxit << endl;
    return result;
}

double NASA_poly1::Hf298() const
{
	const double T = 298.15;
	return H_RT(T) * Rm * T;                    /* J/mol */
}

}  // namespace mw
