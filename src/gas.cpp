//-----------------------------------------------------------------------

// <copyright file="gas.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "constants.h"
#include "gas.h"
#include "utils.h"
#include "species.h"

namespace mw {

double Gas::X(const std::string& k) const
{
	return m_X.at(k);
}

double Gas::X_dry(const std::string& k) const
{
    auto xwet = X(k);

    if (k == "H2O")
        return xwet;

    if (m_X.find("H2O") != m_X.end()) {
        auto xH2O = X("H2O");
        return xwet/(1.-xH2O);
    }

    return xwet;
}

void Gas::set_X(const str_to_dbl& x)
{
	copy_value_or_zero(x,species(),m_X);
	normalize(m_X);
}

double Gas::Y(const std::string& k) const
{
	return Species::mwt(k)*X(k)/mmw();
}

double Gas::Y_dry(const std::string& k) const
{
    auto ywet = Y(k);
    if (k == "H2O")
        return ywet;

    if (m_X.find("H2O") != m_X.end()) {
        auto yH2O = Y("H2O");
        return ywet/(1.-yH2O);
    }

    return ywet;
}

str_to_dbl Gas::Y() const
{
    str_to_dbl ty;
    for (auto& p : m_X)
        ty[p.first] = Y(p.first);

    return ty;
}

void Gas::set_Y(const str_to_dbl& y)
{
	str_to_dbl yy;
	copy_value_or_zero(y,species(),yy);
	normalize(yy);

	double yi_Mi = 0.;
	for (auto& k : yy) yi_Mi += k.second/Species::mwt(k.first);

	m_X.clear();
	for (auto& k : yy) m_X[k.first] = k.second/Species::mwt(k.first)/yi_Mi;
}

double Gas::mmw() const
{
	double mw = 0.0;
	for (auto& x : m_X) mw += Species::mwt(x.first)*x.second;
	return mw;
}

double Gas::R() const                   /* gas constant: J/(kg*K) */
{
    double mwt = mmw() * 1.E-3;
    return mw::Rm / mwt;
}

double Gas::density() const                      /* kg/m3 */
{
    return m_P / R() / m_T;
}

double Gas::normDensity() const                  /* kg/m3 bei 101325Pa and 0°C */
{
    return 101325. / R() / 273.15;
}


std::vector<std::string> Gas::species() const
{
	static const std::vector<std::string> v = {"CO2","H2O","O2","N2","Ar","SO2"};
	return v;
}


double Gas::dynamicViscosity() const
{
    using spec = mw::Species;
    auto s = double {0};
    auto s1 = double {0};
    double eta;
    for (auto& x : m_X) {
        eta = spec::dynamicGasViscosity(x.first, m_T);
        s += x.second*eta;
        s1 += x.second/eta;
    }
    return (s + 1/s1)/2;
}

double Gas::thermalConductivity() const
{
    using spec = mw::Species;
    auto s = double {0};
    auto s1 = double {0};
    double lam;
    for (auto& x : m_X) {
        lam = spec::thermalGasConductivity(x.first, m_T);
        s += x.second*lam;
        s1 += x.second/lam;
    }
    return (s + 1/s1)/2;

}

}  // namespace mw

