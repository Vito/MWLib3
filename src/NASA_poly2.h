//-----------------------------------------------------------------------

// <copyright file="NASA_poly2.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  NASA_POLY2_INC
#define  NASA_POLY2_INC

#include "NASA_poly1.h"

namespace mw {

class NASA_poly2
{
public:
    NASA_poly2()
    {}

    NASA_poly2(double Tmin,
               double Tmax,
               double Tmid,
               double alow[7],
               double ahigh[7])
    : m_nlow(Tmin,Tmid,alow),
      m_nhigh(Tmid,Tmax,ahigh)
    {}

    void set_T_minmax(double Tmin,
                      double Tmax,
                      double Tmid)
    {
        m_nlow.set_T_minmax(Tmin,Tmid);
        m_nhigh.set_T_minmax(Tmid,Tmax);
    }

    void set_a(double alow[7], double ahigh[7])
    {
        m_nlow.set_a(alow);
        m_nhigh.set_a(ahigh);
    }

  double Cp_R(double T) const;
  double S_R(double T) const;
  double H_RT(double T) const;
  double Hf298() const;

  double T_from_H_R(double h_R_set /*J/mol / (J/mol*K) = K*/);

  const NASA_poly1& poly1Low() const { return m_nlow; }
  const NASA_poly1& poly1High() const { return m_nhigh; }

private:
  const NASA_poly1* npoly(double T) const;

  NASA_poly1 m_nlow;
  NASA_poly1 m_nhigh;
};

}  // namespace mw

#endif   /* ----- #ifndef NASA_POLY2_INC  ----- */
