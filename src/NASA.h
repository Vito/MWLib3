//-----------------------------------------------------------------------

// <copyright file="NASA.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  NASA_INC
#define  NASA_INC

#include <map>
#include <vector>

#include "gas.h"
#include "NASA_poly2.h"

namespace mw {

class NASA : public Gas
{
public:
	NASA();
    virtual ~NASA() {}

    virtual std::unique_ptr<Gas> clone() const override;

    double cp_mole() override;
    double cp_mass() override;

protected:
  virtual double H_mass_impl();
  virtual double T_from_H_mass_impl(double hmass /*J/kg*/);


  NASA_poly1 nasaPoly1MixtureLow();      // NASA_poly1 for gas mixture and lower temperatures
  NASA_poly1 nasaPoly1MixtureHigh();     // NASA_poly1 for gas mixture and higher temperature

private:
	static std::vector<double> get_NASA_params(const std::string& k);

	static std::map<std::string,std::vector<double>>
	read_NASA_params();

	std::map<std::string,NASA_poly2> m_poly2;
};

}  // namespace mw

#endif   /* ----- #ifndef NASA_INC  ----- */
