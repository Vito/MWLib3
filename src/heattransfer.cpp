//-----------------------------------------------------------------------

// <copyright file="heattransfer.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:17:49 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "heattransfer.h"

namespace heattransfer {

namespace fluegaspipes {

double Num_Laminar(double Re, double Pr, double di, double L)
{
    auto Num_v1 = double {3.66};
    auto Num_v2 = 1.615 * pow(Re*Pr*di/L, 1./3.);
    auto Num_v3 = pow(2./(1.+22.*Pr), 1./6.) * sqrt(Re*Pr*di/L);
    auto Num_vLam = pow(pow(Num_v1, 3) + pow(0.7, 3) + pow(Num_v2-0.7, 3) + pow(Num_v3, 3), 1./3.); // const wall temperature boundary condition

    auto Num_q1 = 4.364;
    auto Num_q2 = 1.953*pow(Re*Pr*di/L, 1./3.);
    auto Num_q3 = 0.924*pow(Pr, 1./3.)*sqrt(Re*di/L);
    auto Num_qLam = pow(pow(Num_q1, 3) + pow(0.6, 3) + pow(Num_q2-0.6, 3) + pow(Num_q3, 3), 1./3.);  // const heat flux boundary condition with hydrotinamic and thermic einlauf

    auto Num_Lam = (3*Num_vLam + 2*Num_qLam)/5;

    //auto Num_Lam = Num_vLam;

    return Num_Lam;
}

double Num_Turbulent(double Re, double Pr, double di, double L) {

    //auto zeta = pow(1.8 * log10(Re) - 1.5, -2);

   auto zeta = pow(1.82 * log10(Re) - 1.64, -2);   // the constants should be 1.8 and 1.50 laut old VDI this gives
                               // some better agreement with KED


    auto x = zeta/8*Re*Pr;
    auto y = 1+12.7*sqrt(zeta/8) * (pow(Pr, 2./3.)-1);
    auto z = (1 + pow(di/L, 2./3.));


    auto Num_T = x/y*z;

    return Num_T;
}

double Num(double Re, double Pr, double di, double L)
{
    if (Re <= 2300) {
        return Num_Laminar(Re, Pr, di, L);
    } else if (10000 <= Re) {
        return Num_Turbulent(Re, Pr, di, L);
    } else {
        auto gamma = (Re-2300.)/(10000 - 2300);
        auto Num_Lam = Num_Laminar(2300, Pr, di, L);
        auto Num_Tur = Num_Turbulent(10000, Pr, di, L);
        auto Nu = (1.-gamma)*Num_Lam + gamma*Num_Tur;
        return Nu;
    }
}

double SinglePassFctor::operator()(double T)
{

  auto Tm = (m_Tfg_in + T) / 2;     // average temperature;

  fg_in->set_T(m_Tfg_in);
  fg_out->set_T(T);
  fg_out->set_P(fg_in->P() - m_dP);

  fg_m->set_T(Tm);
  fg_m->set_P((fg_in->P() + fg_out->P())/2);

  auto eta_m = fg_m->dynamicViscosity();
  auto lambda_m = fg_m->thermalConductivity();
  auto cp_m = fg_m->cp_mass();
  auto rho_m = fg_m->density();
  auto nu_m = eta_m / rho_m;

  auto a_m = lambda_m/rho_m/cp_m;

  auto Pr = nu_m / a_m;
  auto Re = phi * di / eta_m;

  auto Nu = Num(Re, Pr, di, L);

  auto alpha_i = Nu * lambda_m / di;

  auto deltaT1 = fg_in->T() - m_Tmedium_mid;
  auto deltaT2 = fg_out->T() - m_Tmedium_mid;

  auto logT = (deltaT1 - deltaT2) / log(deltaT1/deltaT2);

  auto lambdaSteel = 57.;
  auto Q_heat = double {0};

  auto k = 1 / (1/((alpha_i+m_alpha_i_rad)*Ai) + log(da/di)/(2*PI*nPipes*L*lambdaSteel) + 1/(m_alpha_a*Aa)) / Ai;
  k *= clearnessFactor;   // add clearness factor
  Q_heat = k * Ai * logT;

  auto h_in = fg_in->H_mass();
  auto h_out = fg_out->H_mass();
  auto Q_enthalpy = m_mdotGas * (h_in - h_out) - radiationLossesWatt;

  return (Q_heat - Q_enthalpy)/1000;
}


std::unique_ptr<mw::Gas> solveAndGetGasOutTemperature(const SinglePass& pass,
                                    double mdotFg,
                                    const mw::Gas& fg_input,
                                    //double Tfg_in_K,
                                    double Tmedium_in_K,
                                    double Tmedium_out_K)
{
    if (fg_input.T() <= Tmedium_in_K)
        return fg_input.clone();

    using namespace std;                          // Help ADL of std functions.
    using namespace boost::math::tools;           // For bracket_and_solve_root.

    //int exponent;
    //frexp(x, &exponent);                          // Get exponent of z (ignore mantissa).
    //T guess = ldexp(1., exponent/3);              // Rough guess is to divide the exponent by three.
    double factor = 1.2;                                 // How big steps to take when searching.

    const boost::uintmax_t maxit = 30;            // Limit to maximum iterations.
    boost::uintmax_t it = maxit;                  // Initally our chosen max iterations, but updated with actual.
    bool is_rising = true;                        // So if result if guess^3 is too low, then try increasing guess.
    int digits = std::numeric_limits<double>::digits;  // Maximum possible binary digits accuracy for type T.
    // Some fraction of digits is used to control how accurate to try to make the result.
    int get_digits = digits - 3;                  // We have to have a non-zero interval at each step, so
                                                  // maximum accuracy is digits - 1.  But we also have to
                                                  // allow for inaccuracy in f(x), otherwise the last few
                                                  // iterations just thrash around.

    eps_tolerance<double> tol(get_digits);             // Set the tolerance.

    auto alpha_i_rad = double {0};
    double alpha_i_rad_prev;

    auto alpha_a = double {1000.};
    double alpha_a_prev;

    auto dP = double {0};
    double dP_prev;


    auto logT_a = double {4.5};
    auto Aa = pass.Aa_m2();
    double Q;

    auto fg_in = fg_input.clone();

    auto Tfg_in_K = fg_in->T();
    auto Tmedium = (Tmedium_in_K + Tmedium_out_K) / 2;

    std::pair<double, double> r;
    double T = Tmedium + 1;       // need this as starting point for alpha_i_rad
    double Twall = (Tmedium + sqrt(Tfg_in_K*Tmedium))/2;

    fg_in->set_T(Tfg_in_K);
    auto h1 = fg_in->H_mass();

    auto fg_out = fg_in->clone();

    auto phi = mdotFg / pass.Aq_m2();

    do {
        alpha_i_rad_prev = alpha_i_rad;
        alpha_a_prev = alpha_a;
        dP_prev = dP;


        auto p_bar = (fg_in->P() + fg_out->P())/2  / 1E5;
        auto psglCO2 = p_bar * fg_in->X("CO2") * pass.sgl_m();
        auto psglH2O = p_bar * fg_in->X("H2O") * pass.sgl_m();


        auto fctorTgout = SinglePassFctor(pass,
                     mdotFg,
                     *fg_in,
                     Tfg_in_K,
                     Tmedium_in_K,
                     Tmedium_out_K,
                     alpha_i_rad,
                     alpha_a,
                     dP);

        auto guess = Tmedium;
        r = bracket_and_solve_root(fctorTgout, guess, factor, is_rising, tol, it);
        T = r.first + (r.second-r.first)/2;

        fg_out->set_T(T);
        auto h2 = fg_out->H_mass();

        Q = mdotFg*(h1-h2);

        // Berechnung vom aeusseren Waermeuebergangskoeffizient:
        // Verdampfer: Calc.alpha_aussen = 1.95*(Wärmestromdichte^0.72) * (Druck^0.24) -- noch nicht implementiert
        // sonst: alpha aussen so dass die Rohrwandtemperatur 5K ueber die Wassertemperatur liegt
        alpha_a = Q/Aa/logT_a;


        // now recalculate the wall temperature
        auto lambdaSteel = double {57};
        auto k_wall_out = 1 / (log(pass.da_m()/pass.di_m())/(2*PI*pass.numPipes()*pass.L_m()*lambdaSteel) + 1/(alpha_a*pass.Aa_m2())) / pass.Ai_m2();

        Twall = Tmedium + Q/pass.Ai_m2()/k_wall_out;

        auto logT = logTempDifference(Tfg_in_K, T, Tmedium, Tmedium);
        alpha_i_rad = qRadiation(psglCO2,psglH2O,sqrt(Tfg_in_K*T), Twall, EMISSIVITY_WALL)/ logT;

        // recalculate pressure drop
        dP = 0;
        auto wIn = phi / fg_in->density();
        auto wOut = phi / fg_out->density();
        auto rhoAvg = 0.5*(fg_in->density() + fg_out->density());
        auto wAvg = phi/rhoAvg;

        auto zetaFriction = pow(1/(1.14 + 2*log(1/pass.relRoughness())), 2);

        dP += 0.5 * pass.zetaFactorIn() * fg_in->density() * wIn * wIn;
        dP += 0.5 * pass.zetaFactorOut() * fg_out->density() * wOut * wOut;
        dP += zetaFriction * pass.L_m()/pass.di_m() * rhoAvg * wAvg * wAvg / 2;

        fg_out->set_P(fg_in->P() -dP);

    } while (   fabs(alpha_a-alpha_a_prev)>1
             || fabs(alpha_i_rad-alpha_i_rad_prev)>0.1
             || fabs(dP-dP_prev) > 1);

    return fg_out;
}


std::unique_ptr<mw::Gas> solveAndGetGasOutTemperature(const std::vector<SinglePass>& passes,
                                    double mdotFg,
                                    const mw::Gas& fg_in,
                                    double Tmedium_in_K,
                                    double Tmedium_out_K)
{
    auto fg = fg_in.clone();
    std::unique_ptr<mw::Gas> fg_prev;
    for (size_t i = 0; i < passes.size(); i++) {
        fg_prev = fg->clone();
       fg = solveAndGetGasOutTemperature(passes[i], mdotFg, *fg_prev, Tmedium_in_K, Tmedium_out_K);
    }

    return fg;
}


double InsideWallTempFctor::operator()(double Twall)
{
    auto qRad = qRadiation(m_psglCO2,
                           m_psglH2O,
                           m_Tg,
                           Twall,
                           m_epsilonWall);
    return qRad-m_qRad;
}


double qRadiation(double psglCO2,
                  double psglH2O,
                  double Tg,
                  double Twall,
                  double eWall)
{
    /*
       Shack method
    //CO2
    double q_CO2 = 0, q_H2O = 0;
    double fk = 1;

    if (psglCO2 > 0)
        q_CO2 = eWall*10.35*pow(psglCO2,0.4)*(pow(Tg/100, 3.2) - pow(Tg/Twall,0.65) * pow(Twall/100,3.2));

    //H2O
    if (psglH2O > 0) {
        auto m = 2.32 + 1.37*pow(psglH2O, 1./3.);
        auto q_H2O = eWall*(46.5-84.9*psglH2O)*pow(psglH2O,0.6)*(pow(Tg/100,m) - pow(Tg/Twall,0.45) * pow(Twall/100,m));

    }

    if (psglCO2>0 && psglH2O>0) {
        auto sum_psgl = psglCO2 + psglH2O;
        auto z = psglCO2/sum_psgl;
        fk = 1 + 0.25*sum_psgl/(0.11+sum_psgl)*(1-z)*log(1-z);
    }

    auto q = fk*(q_CO2 + q_H2O);

    return q;
    */


    // VDI method
    double eg, av;
    emiss_CO2_H2O(psglCO2, psglH2O, Tg, Twall, eg, av);

    auto q = 5.67e-8 * eWall/(1-(1-eWall)*(1-av)) * (eg*pow(Tg, 4) - av*pow(Twall, 4));
    return q;
}



double logTempDifference(double Tg_1, double Tg_2,
            double Tmed_1, double Tmed_2)
{
    auto dT1 = Tg_1 - Tmed_1;
    auto dT2 = Tg_2 - Tmed_2;
    return (dT1-dT2)/log(dT1/dT2);
}


double emissCO2(double psgl, double Tg)
{
    static auto b1 = std::array<double,6>
    {
          0.1074,
          0.027237,
          0.058438,
          0.019078,
          0.056993,
          0.0028014
    };

    static auto b2 = std::array<double,6>
    {
         -0.10705,
          0.10127,
          -0.001208,
          0.037609,
          -0.025412,
          0.038826
    };


    static auto b3 = std::array<double,6>
    {
        0.072727,
        -0.043773,
        0.0006558,
       -0.015424,
          0.0026167,
          -0.020198
    };


    static auto b4 = std::array<double,6>
    {    0.036,
         0.3586,
         3.06,
         14.76,
         102.28,
         770.6
    };

    static auto k = std::array<double,6>
    {
        0.036,
        0.3586,
        3.06,
        14.76,
        102.28,
        770.6
    };

    static auto c = std::array<double,3>
     {
        0.27769,
        0.03869,
        1.4249E-5
     };

    auto T = Tg/1000;
    auto T2 = T*T;

    auto Z = c[0] + c[1]*T + c[2]*T2;

    auto s = double {0};
    for (size_t i = 0; i < 6; i++) {
        auto a = b1[i]+b2[i]*T+b3[i]*T2;
       s += a*exp(-k[i]*psgl);
    }

    auto emiss = Z-s;
    return emiss;
}

double emissH2O(double psgl, double Tg)
{
    static auto b005_05 = std::array<double,5>{ 0.43265, -0.1089, 0.3273, -0.043821};
    static auto k005_05 = 3.5829;

    static auto b05_2 = std::array<double,5>{0.66439, -0.17389, 0.4572, -0.1317};
    static auto k05_2 = 0.84652;

    double k;
    std::array<double,5> *bp = nullptr;

    if (psgl < 0.5) {
        k = k005_05;
        bp = &b005_05;
    } else {
        k = k05_2;
        bp = &b05_2;
    }

    std::array<double,5> &b = *bp;

    auto T = Tg/1000;

    auto Z = b[0] + b[1]*T;
    auto a = b[2] + b[3]*T;
    auto emiss = Z-a*exp(-k*psgl);

    return emiss;
}


void emiss_CO2_H2O(double psglCO2,
                   double psglH2O,
                   double Tg,
                   double Tw,
                   double& eg,
                   double& av)
{
    auto eCO2g = emissCO2(psglCO2, Tg);
    auto eCO2w = emissCO2(psglCO2*Tw/Tg, Tw);
    auto avCO2 = pow(Tg/Tw, 0.65) * eCO2w;

    auto eH2Og = emissH2O(psglH2O, Tg);
    auto eH2Ow = emissH2O(psglH2O*Tw/Tg, Tw);
    auto avH2O = pow(Tg/Tw, 0.45) * eH2Ow;

    eg = eCO2g + eH2Og - eCO2g*eH2Og;
    av = avCO2 + avH2O - eCO2w*eH2Ow;
}

}   // namespace fluegaspipes

}   // namespace heattransfer
