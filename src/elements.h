//-----------------------------------------------------------------------

// <copyright file="elements.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef MW_ELEMENTS_INC
#define MW_ELEMENTS_INC

#include "common_types.h"

namespace mw {
  
class Elements 
{
public:
  static double mwt(const std::string& m);
  
private:
  static str_to_dbl create_mwts();
};

}  // namespace mw

#endif  // MW_ELEMENTS_INC
