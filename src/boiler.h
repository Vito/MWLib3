//-----------------------------------------------------------------------

// <copyright file="boiler.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#pragma once

#ifndef MW_BOILER_H
#define MW_BOILER_H

namespace mw {

class Boiler
{
public:
    Boiler();
    ~Boiler();

private:
    double m_power;       // Watt
};

}  // namespace mw

#endif  // MW_BOILER_H
