//-----------------------------------------------------------------------

// <copyright file="gas.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  GAS_INC
#define  GAS_INC

#include <map>
#include <memory>
#include <vector>

#include "common_types.h"

namespace mw {

class Gas
{
public:
  virtual  ~Gas() {}

  virtual std::unique_ptr<Gas> clone() const = 0;

  double H_mass()                                /* J/kg */
  {
    return H_mass_impl();
  }

  double deltaH_mass(double t2Celsius, double t1Celsius)
  {
      double Told = m_T;

      double T2 = t2Celsius + 273.15;
      set_T(T2);
      double h2 = H_mass();

      double T1 = t1Celsius + 273.15;
      set_T(T1);
      double h1 = H_mass();

      set_T(Told);

      return h2-h1;
  }

  virtual double cp_mole() = 0;
  virtual double cp_mass() = 0;

  double T_from_H_mass(double H_mass /*J/kg*/)
  {
      return T_from_H_mass_impl(H_mass);
  }

  double dynamicViscosity() const;              /* Pas */
  double thermalConductivity() const;           /* W/(m*K) */


  double X(const std::string& k) const;         /* mole fraction */
  double X_dry(const std::string& k) const;
  void set_X(const str_to_dbl& x);
  str_to_dbl X() const { return m_X; }


  double Y(const std::string& k) const;         /* mass fraction */
  double Y_dry(const std::string& k) const;
  void set_Y(const str_to_dbl& y);
  str_to_dbl Y() const;



  double mmw() const;                           /* mean molar weight (g/mol) or (kg/kmol)*/

  double R() const;                             /* gas constant: J/(kg*K) */

  double density() const;                       /* kg/m3 */

  double normDensity() const;                   /* kg/m3 bei 101325Pa and 0°C */


  inline void set_TP(double T, double P)
  {
      m_T = T;
      m_P = P;
  }

  inline double T() const { return m_T; }
  inline void set_T(double T)
  {
      m_T = T;
  }

  inline double P() const { return m_P; }
  inline void set_P(double P)
  {
      m_P = P;
  }

  double RT() const { return R()*T(); }          /* J/kg */

protected:
  virtual double H_mass_impl() = 0;
  virtual double T_from_H_mass_impl(double hmass /*J/kg*/) = 0;

  virtual std::vector<std::string> species() const;

  str_to_dbl m_X;                               /* mole fractions */
  double m_T;                                   /* temperature in Kelvin */
  double m_P;                                   /* pressure in Pa */
};

}  // namespace mw

#endif   /* ----- #ifndef GAS_INC  ----- */
