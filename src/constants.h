//-----------------------------------------------------------------------

// <copyright file="constants.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef  CONSTANTS_INC
#define  CONSTANTS_INC

namespace mw {

constexpr double Rm = 8.3144598;                    // J/(mol*K)
constexpr double one_bar = 1.0E5;               // 1.0E5 Pa 
constexpr double one_atm = 1.01325E5;           // 101325 Pa

 }  // namespace mw

#endif   /* ----- #ifndef CONSTANTS_INC  ----- */
