//-----------------------------------------------------------------------

// <copyright file="NASA_poly2.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "NASA_poly2.h"

namespace mw {

double NASA_poly2::Cp_R(double T) const
{
  return npoly(T)->Cp_R(T);
}

double NASA_poly2::S_R(double T) const
{
  return npoly(T)->S_R(T);
}

double NASA_poly2::H_RT(double T) const
{
  return npoly(T)->H_RT(T);
}

double NASA_poly2::Hf298() const
{
  return npoly(298.15)->Hf298();
}

const NASA_poly1* NASA_poly2::npoly(double T) const
{
  return (T<m_nlow.Tmax()) ? &m_nlow : &m_nhigh;
}


double NASA_poly2::T_from_H_R(double h_R_set /*J/kg*/)
{
    if (h_R_set < m_nlow.H_R_max()) {
        return m_nlow.calc_T_from_H_R(h_R_set);
    } else {
        return m_nhigh.calc_T_from_H_R(h_R_set);
    }
}

}  // namespace mw
