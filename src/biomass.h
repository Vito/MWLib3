//-----------------------------------------------------------------------

// <copyright file="biomass.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef MW_BIOMASS_INC
#define MW_BIOMASS_INC

#include "air.h"
#include "common_types.h"
#include "NASA.h"

namespace mw {

class Biomass 
{
public:
  Biomass()
      : m_A_wf(0.),
        m_W(0.)
  {}

  Biomass(const str_to_dbl& waf,
          double a_wf = 0.,
          double w = 0.)
      : m_Y_waf(waf),
        m_A_wf(a_wf),
        m_W(w)
  {}

  void set_Y_waf(const str_to_dbl& Y_waf) { m_Y_waf = Y_waf; }
  void set_W(double w) { m_W = w; }
  void set_A_wf(double a_wf) { m_A_wf = a_wf; }

  double Y_waf(const std::string& k) const;   /* mass fraction water-ash free */
  double Y_wf(const std::string& k) const;    /* mass fraction water-free */
  double Y(const std::string& k) const;       /* mass fraction wet */

  // return mol(flue gas) / mole C (HxOySz)
  double fluegas_from_lambda(double lambda,
                             const Air& air,
                             str_to_dbl& nu_fluegas);

  // return mol(flue gas) / kg (biomass wet)
  double fluegas_from_O2d(double x_O2dry,
                          const Air& air,
                          str_to_dbl& nu_fluegas);

  // return mol(flue gas) / kg (biomass wet)
  double fluegas_from_O2w(double x_O2wet,
                          const Air& air,
                          str_to_dbl& nu_fluegas);

  double GCV_dry() const;  // J/kg - Gross Calorific Value per kg dry basis
  double NCV_wet() const;  // J/kg - Net Calorific Value per kg wet basis
  double NCV_wet_KED() const;  // J/kg - Net Calorific Value per kg wet basis by KED Method

  double lambda_from_O2dry(double x_O2dry, const Air& air);
  double lambda_from_O2wet(double x_O2wet, const Air& air);

  double moleO2stoich_per_moleC() const;
  double massO2stoich_per_massFuel();

private:
  double cx() const;
  double cy() const;
  double cz() const;
  double cA() const;
  double moleH2O_per_moleC() const;               /* mole H2O(moisture) per mole C */


  str_to_dbl m_Y_waf;                             /* water-ash-free */
  double m_A_wf;                                  /* ash: water-free */
  double m_W;                                     /* moisture */
};
  
}  // namespace mw

#endif  // MW_BIOMASS_INC
