//-----------------------------------------------------------------------

// <copyright file="biomass.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "biomass.h"
#include "elements.h"
#include "species.h"

namespace mw {

double Biomass::Y_waf(const std::string& k) const
{
  return m_Y_waf.at(k);
}

double Biomass::Y_wf(const std::string& k) const
{
  if (k == "A")
    return m_A_wf;
  else
    return m_Y_waf.at(k)*(1.-m_A_wf);
}


double Biomass::Y(const std::string& k) const
{
  if (k == "A")
    return m_A_wf*(1.-m_W);
  else if (k == "W")
    return m_W;
  else 
    return m_Y_waf.at(k)*(1.-m_A_wf)*(1.-m_W);
}


double Biomass::cx() const
{
    using el = Elements;
    return Y_waf("H")/Y_waf("C")*el::mwt("C")/el::mwt("H");
}


double Biomass::cy() const
{
    using el = Elements;
    return Y_waf("O")/Y_waf("C")*el::mwt("C")/el::mwt("O");
}


double Biomass::cz() const
{
    using el = Elements;
    return Y_waf("S")/Y_waf("C")*el::mwt("C")/el::mwt("S");
}


double Biomass::cA() const
{
  return 1. + cx()/4 + cz() - cy()/2;
}


double Biomass::fluegas_from_O2d(double x_O2dry,
                        const Air& air,
                        str_to_dbl& nu_fluegas)
{
  double lambda = lambda_from_O2dry(x_O2dry, air);
  return fluegas_from_lambda(lambda, air, nu_fluegas);
}


double Biomass::fluegas_from_O2w(double x_O2wet,
                         const Air& air,
                         str_to_dbl& nu_fluegas)
{
  double lambda = lambda_from_O2wet(x_O2wet, air);
  return fluegas_from_lambda(lambda, air, nu_fluegas);
}


double Biomass::fluegas_from_lambda(double lambda,
                           const Air& air,
                           str_to_dbl& nu_fluegas)
{
  //using sp = Species;
  //using el = Elements;

  double X1 = air.X("N2")/air.X("O2");
  double X2 = air.X("H2O")/air.X("O2");
  double X3 = air.X("Ar")/air.X("O2");  // Argon

  double A = cA();

  double W = moleH2O_per_moleC();

  str_to_dbl nu;
  nu["CO2"] = 1.;
  nu["H2O"] = cx()/2 + lambda*A*X2 + W;
  nu["SO2"] = cz();
  nu["O2"] = (lambda - 1)*A;
  nu["N2"] = lambda*A*X1;
  nu["Ar"] = lambda*A*X3;


  double nmole = 0.;
  for (auto& n : nu) nmole += n.second;

  nu_fluegas = nu;
  return nmole;
}


double Biomass::GCV_dry() const
{
    double gcv = 0.3491*Y_wf("C")+1.1783*Y_wf("H")+0.1005*Y_wf("S")-0.0151*Y_wf("N")-0.1034*Y_wf("O")-0.0211*Y_wf("A");
    return gcv*1.0E8;
}

double Biomass::NCV_wet() const
{
    double gcvdry = GCV_dry();
    double ncv = gcvdry*(1.0-m_W)-(2.444*m_W+2.444*Y_wf("H")*8.936*(1.-m_W))*1.0E6;
    return ncv;
}

double Biomass::NCV_wet_KED() const  // J/kg - Net Calorific Value per kg wet basis by KED Method
{
    double y_C = Y("C");
    double y_H = Y("H");
    double y_S = Y("S");
    double y_N = Y("N");
    double y_O = Y("O");
    double y_W = Y("W");
    double ncv_kJ_kg = 34800*y_C + 93800*y_H + 10460*y_S + 6280*y_N - 10800*y_O - 2450*y_W;
    return ncv_kJ_kg*1000;
}


double Biomass::lambda_from_O2dry(double x_O2dry, const Air& air)
{
    double A = cA();
    double z = cz();
    double X1 = air.X("N2")/air.X("O2");
    double X3 = air.X("Ar")/air.X("O2");

    double num = A + x_O2dry*(1+z-A);
    double den = A*(1-x_O2dry*(1+X1+X3));
    return num/den;
}


double Biomass::lambda_from_O2wet(double x_O2wet, const Air& air)
{
    //using sp = mw::Species;
    //using el = mw::Elements;

    double A = cA();
    double x = cx();
    double z = cz();
    double W = moleH2O_per_moleC();
    double X1 = air.X("N2")/air.X("O2");
    double X2 = air.X("H2O")/air.X("O2");
    double X3 = air.X("Ar")/air.X("O2");

    double num = A + x_O2wet*(1.+x/2+z+W-A);
    double den = A*(1-x_O2wet*(1.+X1+X2+X3));
    return num/den;
}

double Biomass::moleH2O_per_moleC() const
{
    using sp = Species;
    using el = Elements;
    return Y("W")/Y("C")*el::mwt("C")/sp::mwt("H2O");
}


double Biomass::moleO2stoich_per_moleC() const
{
    return cA();
}

double Biomass::massO2stoich_per_massFuel()
{
    return moleO2stoich_per_moleC() * Species::mwt("O2")
            / Elements::mwt("C") * Y("C");
}

}  // namespace mw
