//-----------------------------------------------------------------------

// <copyright file="interpolation.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include <vector>
#include <stdexcept>

namespace mw {


class Interpolation
{
public:
    Interpolation(const std::vector<double>& x_,
                  const std::vector<double>& y_)
        : x{x_}, y{y_}
    {
        if (x.size() != y.size())
            throw std::runtime_error("Error in Interpolation::ctor : x and y are not of the same size!");
        //  sort the x in ascending order
        for (unsigned int i = 0; i < x.size(); i++)
            for (unsigned int j = i+1; j < x.size(); j++)
                if (x[i] > x[j]) {
                    double t = x[i];
                    x[i] = x[j];
                    x[j] = t;

                    t = y[i];
                    y[i] = y[j];
                    y[j] = t;
                }
    }

    double operator()(double x0)
    {
        unsigned int i = 0;
        for (; x0>x[i] && i<x.size(); i++) ;

        if (x.size() <= i) return y[x.size()];
    }

private:
    std::vector<double> x;
    std::vector<double> y;
};

}  // namespace mw

#endif // INTERPOLATION_H
