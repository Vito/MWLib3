//-----------------------------------------------------------------------

// <copyright file="furnace_biomass.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#pragma once

#ifndef MW_FURNACE_BIOMASS_H
#define MW_FURNACE_BIOMASS_H

#include "biomass.h"
#include "air.h"
#include "fluegas.h"

namespace mw {

class Furnace_biomass
{
public:
    Furnace_biomass() {};
    ~Furnace_biomass() {};

    void set_biomass(const Biomass& biomass) { m_biomass = biomass; }
    Biomass& biomass() { return m_biomass; }

    void set_air(const Air& air) { m_air = air; }
    Air& air() { return m_air; }

    void set_Trecirculation(double T) { m_Trecirculation = T; }
    double Trecirculation() const { return m_Trecirculation; }

    void set_lambda(double lambda) { m_lambda = lambda; }
    double lambda() const { return m_lambda; }
    void set_O2dry(double x_O2dry);
    void set_O2wet(double x_O2wet);


private:
    Biomass m_biomass;
    Air m_air;
    double m_Trecirculation;  // Kelvin
    double m_lambda;
};

}  // namespace mw


#endif  // MW_FURNACE_BIOMASS_H
