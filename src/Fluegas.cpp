//-----------------------------------------------------------------------

// <copyright file="Fluegas.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "fluegas.h"

namespace mw {


Fluegas::Fluegas()
{
}

std::unique_ptr<Gas> Fluegas::clone() const {
    auto p = std::unique_ptr<Fluegas>(new Fluegas);
    if (p) {
        *p = *this;
        return std::move(p);
    } else {
        return nullptr;
    }
}

}  // namespace mw
