//-----------------------------------------------------------------------

// <copyright file="common_types.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef MW_COMMON_TYPES_INC
#define MW_COMMON_TYPES_INC

#include <array>
#include <map>
#include <string>

using str_to_dbl = std::map<std::string, double>;

#endif  // MW_COMMON_TYPES_INC
