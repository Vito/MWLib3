//-----------------------------------------------------------------------

// <copyright file="utils.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "utils.h"

namespace mw {

void normalize(str_to_dbl& x)
{
	double s = 0.;
	for (auto& k : x) s += k.second;
	for (auto& k : x) k.second /= s;
}

void copy_value_or_zero(const str_to_dbl& x_in,
                        const std::vector<std::string>& species,
                        str_to_dbl& x_out)
{
	x_out.clear();
	for (auto& k : species)
		x_out[k] = (x_in.find(k) != x_in.end()) ? x_in.at(k) : 0.0;
}

std::string uppercase(const std::string& s)
{
	std::string uc(s);
	for (size_t i=0; i<s.size(); i++) {
		uc[i] = (char) toupper(s[i]);
	}
	return uc;
}

}  // namespace mw
