//-----------------------------------------------------------------------

// <copyright file="fluegas.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#pragma once
#include "NASA.h"

namespace mw {

class Fluegas : public NASA
{
public:
  Fluegas();
  virtual ~Fluegas() {}

  std::unique_ptr<Gas> clone() const override;
};

}  // namespace mw
