//-----------------------------------------------------------------------

// <copyright file="elements.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include "elements.h"

namespace mw {

double Elements::mwt(const std::string& m)
{
	static const auto mwts = create_mwts();
	return mwts.at(m);
}

str_to_dbl Elements::create_mwts()
{
  str_to_dbl mwts;
  mwts["C"] = 12.011;
  mwts["H"] = 1.0079;
  mwts["O"] = 15.999;
  mwts["N"] = 14.007;
  mwts["S"] = 32.065;
  mwts["Ar"] = 39.948;
  return mwts;
}

}  // namespace mw
