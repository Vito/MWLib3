//-----------------------------------------------------------------------

// <copyright file="NASA.cpp" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#include <cmath>

#include <fstream>
#include <sstream>
#include <limits>

#include <boost/math/tools/roots.hpp>

#include "constants.h"
#include "NASA.h"
#include "utils.h"

namespace mw {

NASA::NASA()
{
	for (const auto& k : species()) {
		auto p = get_NASA_params(k);
		double Tmin = p[0];
		double Tmax = p[1];
		double Tmid = p[2];
		double alow[7];
		double ahigh[7];
		for (int i=3; i<10; i++) alow[i-3] = p[i];
		for (int i=10; i<17; i++) ahigh[i-10] = p[i];

		m_poly2[k].set_T_minmax(Tmin,Tmax,Tmid);
		m_poly2[k].set_a(alow,ahigh);
	}
}

std::unique_ptr<Gas> NASA::clone() const
{
    auto p = std::unique_ptr<NASA>(new NASA);
    if (p) {
        *p = *this;
        return std::move(p);
    } else {
        return nullptr;
    }
}

double NASA::H_mass_impl()
{
	double h_mol = 0;
	for (auto& k : m_X) 
        h_mol += k.second*m_poly2[k.first].H_RT(m_T);

    h_mol *= Rm*m_T;
	
    double t_mmw = mmw();
    return h_mol/t_mmw*1000.;
}

double NASA::T_from_H_mass_impl(double hmass)
{
    double h_R_set = hmass / R();

    auto poly1Low = nasaPoly1MixtureLow();
    auto poly1High = nasaPoly1MixtureHigh();

    if (h_R_set < poly1Low.H_R_max())
        return poly1Low.calc_T_from_H_R(h_R_set);
    else
        return poly1High.calc_T_from_H_R(h_R_set);
}

double NASA::cp_mole()
{
    double cp_m = 0;
    for (auto& k : m_X)
        cp_m += k.second*m_poly2[k.first].Cp_R(m_T);

    cp_m *= Rm;
    return cp_m;
}

double NASA::cp_mass()
{
    double t_mmw = mmw()/1000; // kg/mole
    return cp_mole() / t_mmw;
}

std::vector<double> NASA::get_NASA_params(const std::string& k)
{
	static auto coeffs = read_NASA_params();
	return coeffs.at(k);
}


std::map<std::string,std::vector<double>>
NASA::read_NASA_params()
{
	using namespace std;
	map<string, vector<double>> coeff;

    coeff["CO2"] = {200., 6000., 1000.,
           2.356813,           0.0089841299,   -0.0000071220632,
           0.0000000024573008, -1.4288548E-13, -48371.971,
           9.9009035,

           4.6365111,           0.0027414569, -0.00000099589759,
           0.00000000016038666, -9.1619857E-15, -49024.904,
           -1.9348955};

    coeff["O2"] = { 200., 6000., 1000.,
           3.78245636,            -0.00299673415,    0.000009847302,
          -0.00000000968129508,    3.24372836E-12,  -1063.94356,
           3.65767573,

            3.66096083,            0.000656365523,     -0.000000141149485,
            0.0000000000205797658, -1.29913248E-15,    -1215.97725,
            3.41536184 };

    coeff["N2"] = { 200., 6000., 1000.,
            3.53100528,          -0.000123660988, -0.000000502999433,
            0.00000000243530612, -1.40881235E-12, -1046.97628,
            2.96747038,

            2.95257637,             0.0013969004,   -0.000000492631603,
            0.0000000000786010195, -4.60755204E-15, -923.948688,
            5.87188762
            };

    coeff["H2O"] = {200., 6000., 1000.,
            4.1986352,
            -0.0020364017,
            0.0000065203416,
            -0.0000000054879269,
            0.000000000001771968,
            -30293.726,
            -0.84900901,

            2.6770389,
            0.0029731816,
            -0.00000077376889,
            0.000000000094433514,
            -4.2689991E-15,
            -29885.894,
            6.88255 };


    coeff["Ar"] = { 200.0,6000.0,1000.0,
                    2.5,
                    0,
                    0,
                    0,
                    0,
                    -745.375,
                    4.3796749,

                    2.5,
                    0,
                    0,
                    0,
                    0,
                    -745.375,
                    4.3796749};


    // TODO: Check if there are any better coefficients here
    coeff["SO2"] = { 200.0,6000.0,1000.0,
                    0.02500000E+02,0.00000000E+00,0.00000000E+00,0.00000000E+00,0.00000000E+00,
                    -0.07453750E+04,0.04366000E+02,0.02500000E+02,0.00000000E+00,0.00000000E+00,
                    0.00000000E+00,0.00000000E+00,-0.07453750E+04,0.04366000E+02 };

    /* Old coefficients
	coeff["CO2"] = { 200.0,3500.0,1000.0,
					3.85746029E+00,4.41437026E-03,-2.21481404E-06,5.23490188E-10,-4.72084164E-14,
					-4.87591660E+04,2.27163806E+00,2.35677352E+00,8.98459677E-03,-7.12356269E-06,
					2.45919022E-09,-1.43699548E-13,-4.83719697E+04,9.90105222E+00 };

	coeff["O2"] = { 200.0,3500.0,1000.0,
					3.28253784E+00,1.48308754E-03,-7.57966669E-07,2.09470555E-10,-2.16717794E-14,
					-1.08845772E+03,5.45323129E+00,3.78245636E+00,-2.99673416E-03,9.84730201E-06,
					-9.68129509E-09,3.24372837E-12,-1.06394356E+03,3.65767573E+00 };

	coeff["N2"] = { 300.0,5000.0,1000.0,
					0.02926640E+02,0.14879768E-02,-0.05684760E-05,0.10097038E-09,-0.06753351E-13,
					-0.09227977E+04,0.05980528E+02,0.03298677E+02,0.14082404E-02,-0.03963222E-04,
					0.05641515E-07,-0.02444854E-10,-0.10208999E+04,0.03950372E+02 };
	coeff["H2O"] = { 200.0,3500.0,1000.0,
					 3.03399249E+00,2.17691804E-03,-1.64072518E-07,-9.70419870E-11,1.68200992E-14,
					-3.00042971E+04,4.96677010E+00,4.19864056E+00,-2.03643410E-03,6.52040211E-06,
					-5.48797062E-09,1.77197817E-12,-3.02937267E+04,-8.49032208E-01 };
	coeff["Ar"] = { 300.0,5000.0,1000.0,
					0.02500000E+02,0.00000000E+00,0.00000000E+00,0.00000000E+00,0.00000000E+00,
					-0.07453750E+04,0.04366000E+02,0.02500000E+02,0.00000000E+00,0.00000000E+00,
					0.00000000E+00,0.00000000E+00,-0.07453750E+04,0.04366000E+02 };

    // TODO -> set the right coefficients !
	coeff["SO2"] = { 300.0,5000.0,1000.0,
					0.02500000E+02,0.00000000E+00,0.00000000E+00,0.00000000E+00,0.00000000E+00,
					-0.07453750E+04,0.04366000E+02,0.02500000E+02,0.00000000E+00,0.00000000E+00,
					0.00000000E+00,0.00000000E+00,-0.07453750E+04,0.04366000E+02 };
    */
	return coeff;
}


NASA_poly1 NASA::nasaPoly1MixtureLow()
{
  double a[7];
  double minT = 0;
  double maxT = 10000;

  for (unsigned int i = 0; i < 7; i++) {
    a[i] = 0;
    for (auto& k : m_X) {
      NASA_poly1 poly1 = m_poly2[k.first].poly1Low();

      a[i] += k.second * poly1.coeff(i);

      if (minT < poly1.Tmin()) minT = poly1.Tmin();
      if (maxT > poly1.Tmax()) maxT = poly1.Tmax();
    }
  }

  return NASA_poly1(minT, maxT, a);
}

NASA_poly1 NASA::nasaPoly1MixtureHigh()
{
  double a[7];
  double minT = 0;
  double maxT = 10000;

  for (unsigned int i = 0; i < 7; i++) {
    a[i] = 0;
    for (auto& k : m_X) {
      NASA_poly1 poly1 = m_poly2[k.first].poly1High();

      a[i] += k.second * poly1.coeff(i);

      if (minT < poly1.Tmin()) minT = poly1.Tmin();
      if (maxT > poly1.Tmax()) maxT = poly1.Tmax();
    }
  }

  return NASA_poly1(minT, maxT, a);
}


}  // namespace mw
