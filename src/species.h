//-----------------------------------------------------------------------

// <copyright file="species.h" company="Daniel Petrovic">

// Copyright (c) Daniel Petrovic. All rights reserved.

// <author>Daniel Petrovic</author>

// <date>4/5/2018 9:19:04 PM</date>

// </copyright>

//-----------------------------------------------------------------------

#ifndef MW_SPECIES_INC
#define MW_SPECIES_INC

#include "common_types.h"

namespace mw {
  
class Species 
{
public:
  static double mwt(const std::string& k);
  static double R(const std::string& k);    // J/(kg*K) - specific gas constant
  static double dynamicGasViscosity(const std::string& k, double T_Kelvin);   		// Pa*s
  static double thermalGasConductivity(const std::string& k, double T_Kelvin);      // W/(m*K)

private:

  struct QuadraticCorrelation {

      QuadraticCorrelation() {}

      QuadraticCorrelation(double theA, double theB, double theC)
          : a(theA), b(theB), c(theC)
      {}

      double operator()(double T_Kelvin) const
      {
          double t = T_Kelvin;
          double t2 = t*t;
          return a*t2 + b*t + c;
      }

      double a = 0;
      double b = 0;
      double c = 0;
  };


private:
  static std::map<std::string, QuadraticCorrelation> createGasDynamicViskosityMap();
  static std::map<std::string, QuadraticCorrelation> createGasThermalConductivityMap();
  
private:
  static str_to_dbl create_mwts();
};
  
}  // namespace mw


#endif  // MW_SPECIES_INC
